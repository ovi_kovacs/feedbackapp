import React, { Component } from "react";
import withStyels from "@material-ui/core/styles/withStyles";

import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Link from "react-router-dom/Link";
import IconButton from "@material-ui/core/IconButton";
import DeleteIcon from "@material-ui/icons/Delete";
import FavoriteBorderIcon from "@material-ui/icons/FavoriteBorder";
import FavoriteIcon from "@material-ui/icons/Favorite";
import CommentIcon from "@material-ui/icons/Comment";
import axios from "axios";

const styles = {
  card: {
    display: "flex",
    marginBottom: 20,
  },
  image: {
    minWidth: 200,
    objectFit: "cover",
  },
  content: {
    padding: 25,
  },
};

class Feedback extends Component {
  constructor() {
    super();
    this.state = {
      loading: false,
    };
  }

  deleteFeedback = async (feedbackId) => {
    const HEADERS = {
      headers: {
        Authorization: localStorage.FBIdToken ? localStorage.FBIdToken : "",
        "Content-Type": "application/json",
      },
    };
    await axios.delete(`/feedback/${feedbackId}`, HEADERS);
  };

  likeFeedback = async (feedbackId) => {
    const HEADERS = {
      headers: {
        Authorization: localStorage.FBIdToken ? localStorage.FBIdToken : "",
        "Content-Type": "application/json",
      },
    };
    await axios.get(`/feedback/${feedbackId}/like`, HEADERS);
  };

  unlikeFeedback = async (feedbackId) => {
    const HEADERS = {
      headers: {
        Authorization: localStorage.FBIdToken ? localStorage.FBIdToken : "",
        "Content-Type": "application/json",
      },
    };

    await axios.get(`/feedback/${feedbackId}/unlike`, HEADERS);
  };

  render() {
    const {
      classes,
      feedback: {
        body,
        createdAt,
        userHandle,
        userImage,
        feedbackId,
        likeCount,
        commentCount,
        location,
      },
    } = this.props;

    return (
      <Card className={classes.card}>
        <CardMedia
          image={userImage}
          title="Profile image"
          className={classes.image}
        />
        <CardContent className={classes.content}>
          <Typography
            variant="h5"
            component={Link}
            to={`/users/${userHandle}`}
            color="primary"
          >
            {userHandle}
          </Typography>
          <Typography variant="body2" color="textSecondary">
            {createdAt}
          </Typography>
          <Typography variant="h6" color="primary">
            {location}
          </Typography>
          <Typography variant="body1">{body}</Typography>
          {likeCount}
          <IconButton
            onClick={async () => {
              await this.likeFeedback(feedbackId);
              this.props.refreshFeedbacks();
            }}
          >
            <FavoriteIcon />
          </IconButton>
          <IconButton
            onClick={async () => {
              await this.unlikeFeedback(feedbackId);
              this.props.refreshFeedbacks();
            }}
          >
            <FavoriteBorderIcon />
          </IconButton>
          {commentCount}
          <IconButton
            onClick={async () => {
              // this.commentFeedback(feedbackId);
              // this.props.refreshFeedbacks();
            }}
          >
            <CommentIcon />
          </IconButton>
          <IconButton
            onClick={async () => {
              await this.deleteFeedback(feedbackId);
              this.props.refreshFeedbacks();
            }}
          >
            <DeleteIcon />
          </IconButton>
        </CardContent>
      </Card>
    );
  }
}

export default withStyels(styles)(Feedback);

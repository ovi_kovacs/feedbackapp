let db = {
  users: [
    {
      userId: "dsafghasfghasfakfgak",
      email: "user@email.com",
      handle: "user",
      createdAt: "2019-03-14T10:50:52.792Z",
      imageUrl: "image/dsdaskjdhasd/daskdjhas",
      bio: "Hello, my name is user, nice to meet you",
      website: "https://user.com",
      location: "London, UK",
    },
  ],
  feedbacks: [
    {
      userHandle: "user",
      body: "This is a simple feedback",
      createdAt: "2019-03-14T10:50:52.792Z",
      likeCount: 5,
      commentCount: 3,
      userImage: "image/dsdaskjdhasd/daskdjhas",
      location: "Cape May, New Jersey",
    },
  ],
  comments: [
    {
      userHandle: "user",
      feedbackId: "jdhaskjdfsakjfgasjkfgjlsakf",
      body: "nice one mate!",
      createdAt: "2019-03-15T10:59:52.798Z",
    },
  ],
};

const userDetails = {
  credentials: {
    userId: "dsafghasfghasfakfgak",
    email: "user@email.com",
    handle: "user",
    createdAt: "2019-03-14T10:50:52.792Z",
    imageUrl: "image/dsdaskjdhasd/daskdjhas",
    bio: "Hello, my name is user, nice to meet you",
    website: "https://user.com",
    location: "London, UK",
  },
  likes: [
    {
      userHandle: "user",
      feedbackId: "hdjshdjsahdsajdsad1231",
    },
    {
      userHandle: "user",
      feedbackId: "hdjshdjsahdssdasajf345",
    },
  ],
};

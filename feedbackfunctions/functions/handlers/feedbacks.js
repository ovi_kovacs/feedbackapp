const { db } = require("../util/admin");

exports.getAllFeedbacks = (req, res) => {
  db.collection("feedbacks")
    .orderBy("createdAt", "desc")
    .get()
    .then((data) => {
      let feedbacks = [];
      data.forEach((doc) => {
        feedbacks.push({
          feedbackId: doc.id,
          body: doc.data().body,
          location: doc.data().location,
          userHandle: doc.data().userHandle,
          createdAt: doc.data().createdAt,
          userImage: doc.data().userImage,
          commentCount: doc.data().commentCount,
          likeCount: doc.data().likeCount,
        });
      });
      return res.json(feedbacks);
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).json({ error: err.code });
    });
};

exports.postOneFeedback = (req, res) => {
  if (req.body.body.trim() === "") {
    return res.status(400).json({ body: "Body must not be empty" });
  }

  const newFeedback = {
    body: req.body.body,
    location: req.body.location,
    userHandle: req.user.handle,
    createdAt: new Date().toISOString(),
    userImage: req.user.imageUrl,
    likeCount: 0,
    commentCount: 0,
  };

  db.collection("feedbacks")
    .add(newFeedback)
    .then((doc) => {
      res.json({ message: `document ${doc.id} created successfully` });
    })
    .catch((err) => {
      res.status(500).json({ error: "something went wrong" });
      console.error(err);
    });
};

exports.getFeedback = (req, res) => {
  let feedbackData = {};
  db.doc(`/feedbacks/${req.params.feedbackId}`)
    .get()
    .then((doc) => {
      if (!doc.exists) {
        return res.status(404).json({ error: "Feedback not found" });
      }
      feedbackData = doc.data();
      feedbackData.feedbackId = doc.id;
      return db
        .collection("comments")
        .orderBy("createdAt", "desc")
        .where("feedbackId", "==", req.params.feedbackId)
        .get();
    })
    .then((data) => {
      feedbackData.comments = [];
      data.forEach((doc) => {
        feedbackData.comments.push(doc.data());
      });
      return res.json(feedbackData);
    })
    .catch((err) => {
      console.error(err);
      res.status(500).json({ error: err.code });
    });
};

exports.searchFeedbacks = (req, res) => {
  db.collection("feedbacks")
    .where("location", "==", req.params.searchLocation)
    .orderBy("createdAt", "desc")
    .get()
    .then((data) => {
      let feedbacks = [];
      data.forEach((doc) => {
        feedbacks.push({
          feedbackId: doc.id,
          body: doc.data().body,
          location: doc.data().location,
          userHandle: doc.data().userHandle,
          createdAt: doc.data().createdAt,
          userImage: doc.data().userImage,
          commentCount: doc.data().commentCount,
          likeCount: doc.data().likeCount,
        });
      });
      return res.json(feedbacks);
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).json({ error: err.code });
    });
};

exports.commentOnFeedback = (req, res) => {
  if (req.body.body.trim() === "")
    return res.status(400).json({ comment: "Must not be empty" });

  const newComment = {
    body: req.body.body,
    createAt: new Date().toISOString(),
    feedbackId: req.params.feedbackId,
    userHandle: req.user.handle,
    userImage: req.user.imageUrl,
  };

  db.doc(`/feedbacks/${req.params.feedbackId}`)
    .get()
    .then((doc) => {
      if (!doc.exists) {
        return res.status(404).json({ error: "Feedback not found" });
      }
      return doc.ref.update({ commentCount: doc.data().commentCount + 1 });
    })
    .then(() => {
      return db.collection("comments").add(newComment);
    })
    .then(() => {
      res.json(newComment);
    })
    .catch((err) => {
      console.log(err);
      res.status(500).json({ error: err.code });
    });
};

exports.likeFeedback = (req, res) => {
  const likeDocument = db
    .collection("likes")
    .where("userHandle", "==", req.user.handle)
    .where("feedbackId", "==", req.params.feedbackId)
    .limit(1);

  const feedbackDocument = db.doc(`/feedbacks/${req.params.feedbackId}`);

  let feedbackData;

  feedbackDocument
    .get()
    .then((doc) => {
      if (doc.exists) {
        feedbackData = doc.data();
        feedbackData.feedbackId = doc.id;
        return likeDocument.get();
      } else {
        res.status(404).json({ error: "Feedback not found" });
      }
    })
    .then((data) => {
      if (data.empty) {
        return db
          .collection("likes")
          .add({
            feedbackId: req.params.feedbackId,
            userHandle: req.user.handle,
          })
          .then(() => {
            feedbackData.likeCount++;
            return feedbackDocument.update({
              likeCount: feedbackData.likeCount,
            });
          })
          .then(() => {
            return res.json(feedbackData);
          });
      } else {
        return res.status(400).json({ error: "Feedback already liked" });
      }
    })
    .catch((err) => {
      console.error(err);
      res.status(500).json({ error: err.code });
    });
};

exports.unlikeFeedback = (req, res) => {
  const likeDocument = db
    .collection("likes")
    .where("userHandle", "==", req.user.handle)
    .where("feedbackId", "==", req.params.feedbackId)
    .limit(1);

  const feedbackDocument = db.doc(`/feedbacks/${req.params.feedbackId}`);

  let feedbackData;

  feedbackDocument
    .get()
    .then((doc) => {
      if (doc.exists) {
        feedbackData = doc.data();
        feedbackData.feedbackId = doc.id;
        return likeDocument.get();
      } else {
        res.status(404).json({ error: "Feedback not found" });
      }
    })
    .then((data) => {
      if (data.empty) {
        return res.status(400).json({ error: "Feedback not liked" });
      } else {
        return db
          .doc(`/likes/${data.docs[0].id}`)
          .delete()
          .then(() => {
            feedbackData.likeCount--;
            return feedbackDocument.update({
              likeCount: feedbackData.likeCount,
            });
          })
          .then(() => {
            res.json(feedbackData);
          });
      }
    })
    .catch((err) => {
      console.error(err);
      res.status(500).json({ error: err.code });
    });
};

exports.deleteFeedback = (req, res) => {
  const document = db.doc(`/feedbacks/${req.params.feedbackId}`);
  document
    .get()
    .then((doc) => {
      if (!doc.exists) {
        return res.status(404).json({ error: "Feedback not found" });
      }
      if (doc.data().userHandle !== req.user.handle) {
        return res.status(403).json({ error: "Unauthorized" });
      }
      return document.delete();
    })
    .then(() => {
      res.json({ message: "Feedback deleted successfully" });
    })
    .catch((err) => {
      console.error(err);
      return res.status(500).json({ error: err.code });
    });
};

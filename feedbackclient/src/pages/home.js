import React, { Component } from "react";
import Grid from "@material-ui/core/Grid";
import axios from "axios";
import Feedback from "../components/Feedback";
import Profile from "../components/Profile";
import TextField from "@material-ui/core/TextField";
import Autocomplete from "@material-ui/lab/Autocomplete";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";

export default class home extends Component {
  constructor() {
    super();
    this.state = {
      body: "",
      location: "",
      open: false,
      feedbacks: [],
      locations: [],
    };
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleChange = (event) => {
    this.setState({
      [event.target.name]: event.target.value,
    });
  };

  postFeedback = async () => {
    const HEADERS = {
      headers: {
        Authorization: localStorage.FBIdToken ? localStorage.FBIdToken : "",
        "Content-Type": "application/json",
      },
    };

    const newFeedback = {
      body: this.state.body,
      location: this.state.location,
    };
    await axios.post("/feedback", newFeedback, HEADERS);
    this.handleClose();
    this.getFeedbacks();
    this.getLocations();
  };

  filteredFeedbacks = (value) => {
    axios.get(`/feedbacks/${value}`).then((response) => {
      this.setState({ feedbacks: response.data });
    });
  };

  getLocations = async () => {
    await axios.get("/feedbacks").then((response) => {
      let locations = [];
      response.data.forEach((doc) => {
        locations.push(doc.location);
      });
      this.setState({ locations });
    });
  };

  getFeedbacks = async () => {
    console.log("INTRA!!!!");
    await axios.get("/feedbacks").then((response) => {
      this.setState({ feedbacks: response.data });
    });
  };

  componentDidMount() {
    this.getFeedbacks();
    this.getLocations();
  }

  render() {
    const body = (
      <div className="paper">
        <Grid container spacing={2}>
          <Grid item sm={6} xs={12}>
            <p>Please write your feedback below</p>
          </Grid>
          <Grid item sm={6} xs={12} />
          <Grid item sm={6} xs={12}>
            Message
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField name="body" onChange={this.handleChange} />
          </Grid>
          <Grid item sm={6} xs={12}>
            Location
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField name="location" onChange={this.handleChange} />
          </Grid>
          <Grid item sm={6} xs={12} />
          <Grid item sm={6} xs={12}>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              // disabled={loading}
              onClick={this.postFeedback}
            >
              Post
            </Button>
          </Grid>
        </Grid>
      </div>
    );

    let recentFeedbacksMarkup = this.state.feedbacks ? (
      this.state.feedbacks.map((feedback) => (
        <Feedback refreshFeedbacks={this.getFeedbacks} feedback={feedback} />
      ))
    ) : (
      <p>Loading...</p>
    );
    return (
      <Grid container spacing={2}>
        <Grid item sm={8} xs={12}>
          <Autocomplete
            id="Search"
            freeSolo
            options={this.state.locations}
            onChange={(evt, value) => {
              this.filteredFeedbacks(value);
            }}
            renderInput={(params) => (
              <TextField
                {...params}
                label="Search"
                margin="normal"
                variant="outlined"
              />
            )}
          />
        </Grid>
        <Grid item sm={4} xs={12} />
        <Grid item sm={4} xs={12}>
          <div>
            <Button
              variant="contained"
              color="primary"
              onClick={this.handleOpen}
            >
              Post Feedback
            </Button>
            <Dialog
              open={this.state.open}
              onClose={this.handleClose}
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
            >
              {body}
            </Dialog>
          </div>
        </Grid>
        <Grid item sm={4} xs={12} />
        <Grid item sm={8} xs={12}>
          {recentFeedbacksMarkup}
        </Grid>
        <Grid item sm={4} xs={12}>
          {/* <p>Profile</p> */}
          <Profile />
        </Grid>
      </Grid>
    );
  }
}

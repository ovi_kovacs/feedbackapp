import React from "react";
import { Route, Redirect } from "react-router-dom";
const AuthRoute = ({
  component: Component,
  authenticated,
  updateAuthStatus,
  redirectPath,
  ...rest
}) => (
  <Route
    {...rest}
    render={(props) =>
      authenticated === true ? (
        <Redirect to={redirectPath} />
      ) : (
        <Component {...props} updateAuthStatus={updateAuthStatus} />
      )
    }
  />
);

export default AuthRoute;

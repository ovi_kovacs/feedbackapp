import jwtDecode from "jwt-decode";

export function getActiveUserEmail() {
  const token = localStorage.FBIdToken;
  if (token) {
    console.log("Token exists");
    const decodedToken = jwtDecode(token);
    return decodedToken.email;
  }
  return "";
}

export function isAuthenticated() {
  const token = localStorage.FBIdToken;
  let authenticated = false;
  if (token) {
    console.log("Token exists");
    const decodedToken = jwtDecode(token);
    console.log(decodedToken);
    if (decodedToken.exp * 1000 < Date.now()) {
      window.location.href = "/login";
      authenticated = false;
      console.log("Token expired");
      localStorage.removeItem("FBIdToken");
    } else {
      authenticated = true;
      console.log("Authenticated");
    }
  } else {
    console.log("Token doesn't exist");
  }
  return authenticated;
}

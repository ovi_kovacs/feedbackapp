const functions = require("firebase-functions");

const { db } = require("./util/admin");

const app = require("express")();

const FBAuth = require("./util/fbAuth");

const {
  getAllFeedbacks,
  postOneFeedback,
  getFeedback,
  searchFeedbacks,
  commentOnFeedback,
  likeFeedback,
  unlikeFeedback,
  deleteFeedback,
} = require("./handlers/feedbacks");

const {
  signup,
  login,
  uploadImage,
  addUserDetails,
  getAuthenticatedUser,
  getUserDetails,
} = require("./handlers/users");

//Feedback routes
app.get("/feedbacks", getAllFeedbacks);
app.post("/feedback", FBAuth, postOneFeedback);
app.get("/feedback/:feedbackId", getFeedback);
app.get("/feedbacks/:searchLocation", searchFeedbacks);
app.delete("/feedback/:feedbackId", FBAuth, deleteFeedback);
app.post("/feedback/:feedbackId/comment", FBAuth, commentOnFeedback);
app.get("/feedback/:feedbackId/like", FBAuth, likeFeedback);
app.get("/feedback/:feedbackId/unlike", FBAuth, unlikeFeedback);

// users routes
app.post("/signup", signup);
app.post("/login", login);
app.post("/user/image", FBAuth, uploadImage);
app.post("/user", FBAuth, addUserDetails);
app.get("/user", FBAuth, getAuthenticatedUser);
app.get("/users/:handle", FBAuth, getUserDetails);

exports.api = functions.https.onRequest(app);

exports.onUserImageChange = functions
  .region("europe-west3")
  .firestore.document("/users/{userId}")
  .onUpdate((change) => {
    if (change.before.data().imageUrl !== change.after.data().imageUrl) {
      const batch = db.batch();
      return db
        .collection("feedbacks")
        .where("userHandle", "==", change.before.data().handle)
        .get()
        .then((data) => {
          data.forEach((doc) => {
            const feedback = db.doc(`/feedbacks/${doc.id}`);
            batch.update(feedback, { userImage: change.after.data().imageUrl });
          });
          return db
            .collection("comments")
            .where("userHandle", "==", change.before.data().handle)
            .get();
        })
        .then((data) => {
          data.forEach((doc) => {
            const feedback = db.doc(`/comments/${doc.id}`);
            batch.update(feedback, { userImage: change.after.data().imageUrl });
          });
          return batch.commit();
        });
    } else return true;
  });

exports.onFeedbackDeleted = functions
  .region("europe-west3")
  .firestore.document("/feedbacks/{feedbackId}")
  .onDelete((snapshot, context) => {
    const feedbackId = context.params.feedbackId;
    const batch = db.batch();
    return db
      .collection("comments")
      .where("feedbackId", "==", feedbackId)
      .get()
      .then((data) => {
        data.forEach((doc) => {
          batch.delete(db.doc(`/comments/${doc.id}`));
        });
        return db
          .collection("likes")
          .where("feedbackId", "==", feedbackId)
          .get();
      })
      .then((data) => {
        data.forEach((doc) => {
          batch.delete(db.doc(`/likes/${doc.id}`));
        });
        return batch.commit();
      })
      .catch((err) => {
        console.error(err);
      });
  });

import React, { Component } from "react";
import withStyles from '@material-ui/core/styles/withStyles';
import PropTypes from 'prop-types';
import AppIcon from '../images/monkey2.png';
//MUI Stuff
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/button';
import axios from 'axios';
import {Link} from 'react-router-dom';
import CircularProgress from '@material-ui/core/CircularProgress';

const styles ={
  form: {
    textAlign: 'center'
  },
  image: {
    margin: '20px auto 20px auto'
  },
  pageTitle: {
    margin: '20px auto 20px auto'
  },
  textField: {
    margin: '20px auto 20px auto'
  },
  button: {
    margin: '20px'
    
  }
  
};



export class signup extends Component {
  constructor(){
    super();
    this.state = {
      email:'',
      password: '',
      confirmPassword: '',
      handle: '',
      loading: false,
      errors: {}
    };
  }
  handleSubmit = (event) => {
    event.preventDefault();
    this.setState({
      loading: true
    });
    const newuserData={
      email: this.state.email,
      password: this.state.password,
      confirmPassword: this.state.confirmPassword,
      handle: this.state.handle
    };
    axios.post('/signup',newuserData)
     .then(res => {
       console.log(res.data);
       localStorage.setItem('FBIdToken',`Bearer ${res.data.token}`);
       this.setState({
         loading: false
       });
       this.props.history.push('/'); 
     })
     .catch(err =>{
       this.setState({
         errors: err.response.data,
         loading: false

       })
     });
  };  
  handleChange =(event) => {
    this.setState({
      [event.target.name]: event.target.value
    });
  }
  render() {
    const { classes } = this.props;
    const {errors,loading} = this.state;
    return (
      <Grid container className={classes.form}>
        <Grid item sm/>
        <Grid item sm> 
        <img src={AppIcon} alt="monkey2 img" className={classes.image}/>
        <Typography variant="h2" className={classes.pageTitle}>
           SignUp
        </Typography>

         <form noValidate onSubmit={this.handleSubmit}>
           <TextField 
           id="email" 
           name="email" 
           type="email" 
           label="Email" 
           className={classes.textField} 
          helperText={errors.email}
          error={errors.email ? true : false}
           value={this.state.email} 
           onChange={this.handleChange} 
           fullWidth/>
         </form>

        
         <form noValidate onSubmit={this.handleSubmit}>
           <TextField 
           id="password" 
           name="password" 
           type="password" 
           label="Password" 
           className={classes.textField} 
           helperText={errors.password}
           error={errors.password ? true : false}
           value={this.state.password} 
           onChange={this.handleChange} 
           fullWidth/>
            </form>

<form noValidate onSubmit={this.handleSubmit}>
           <TextField 
           id="confirmPassword" 
           name="confirmPassword" 
           type="password" 
           label="Confirm Password" 
           className={classes.textField} 
           helperText={errors.email}
           error={errors.confirmPassword ? true : false}
           value={this.state.confirmPassword} 
           onChange={this.handleChange} 
           fullWidth/>
            </form>

<form noValidate onSubmit={this.handleSubmit}>
           <TextField 
           id="handle" 
           name="handle" 
           type="text" 
           label="Handle" 
           className={classes.textField} 
           helperText={errors.handle}
           error={errors.handle ? true : false}
           value={this.state.handle} 
           onChange={this.handleChange} 
           fullWidth/>
         
         <Button
         type="submit" 
         variant="contained" 
         color="primary" 
         className={classes.button}
         disabled={loading}>
            SignUp
            
         </Button>
         <br></br>
          <small>Already have an account? Login <Link to="/login"> Here</Link>  </small>
           
         
         </form>
        </Grid>
        <Grid item sm/>
      </Grid>
      
    );
  }
}

signup.propTypes ={
  classes: PropTypes.object.isRequired
}
export default withStyles(styles)(signup);

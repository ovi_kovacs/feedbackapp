import React, { Component } from "react";
import axios from "axios";

import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Typography from "@material-ui/core/Typography";
import Link from "react-router-dom/Link";
import { withStyles } from "@material-ui/core";
import Grid from "@material-ui/core/Grid";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Dialog from "@material-ui/core/Dialog";
import Avatar from "@material-ui/core/Avatar";

const styles = {
  card: {
    display: "flex",
    marginBottom: 20,
  },
  image: {
    minWidth: 200,
    objectFit: "cover",
  },
  content: {
    padding: 25,
  },
};

class Profile extends Component {
  constructor() {
    super();
    this.state = {
      open: false,
      credentials: {},
    };
  }

  handleOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  handleChange = (event) => {
    const state = this.state;
    state.credentials[event.target.name] = event.target.value;
    this.setState(state);
  };

  addUserDetails = () => {
    const HEADERS = {
      headers: {
        Authorization: localStorage.FBIdToken ? localStorage.FBIdToken : "",
        "Content-Type": "application/json",
      },
    };

    const userDetails = {
      bio: this.state.credentials.bio,
      website: this.state.credentials.website,
      location: this.state.credentials.location,
    };

    axios.post("user", userDetails, HEADERS);

    this.handleClose();
  };

  getAuthenticatedUser = () => {
    const HEADERS = {
      headers: {
        Authorization: localStorage.FBIdToken ? localStorage.FBIdToken : "",
        "Content-Type": "application/json",
      },
    };
    axios.get("/user", HEADERS).then((response) => {
      this.setState({ credentials: response.data.credentials });
    });
  };

  componentDidMount() {
    this.getAuthenticatedUser();
  }

  render() {
    const body = (
      <div className="paper">
        <Grid container spacing={2}>
          <Grid item sm={6} xs={12}>
            Bio
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField name="bio" onChange={this.handleChange} />
          </Grid>
          <Grid item sm={6} xs={12}>
            Website
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField name="website" onChange={this.handleChange} />
          </Grid>
          <Grid item sm={6} xs={12}>
            Location
          </Grid>
          <Grid item sm={6} xs={12}>
            <TextField name="location" onChange={this.handleChange} />
          </Grid>
          <Grid item sm={6} xs={12} />
          <Grid item sm={6} xs={12}>
            <Button
              type="submit"
              variant="contained"
              color="primary"
              onClick={this.addUserDetails}
            >
              Save
            </Button>
          </Grid>
        </Grid>
      </div>
    );

    const { classes } = this.props;

    const { credentials } = this.state;

    return (
      <Card className={classes.card}>
        {/* <CardMedia
          image={credentials.imageUrl}
          title="Profile image"
          className={classes.image}
        /> */}
        <CardContent className={classes.content}>
          <Avatar src={credentials.imageUrl} />
          <Typography
            variant="h5"
            component={Link}
            to={`/users/${credentials.handle}`}
            color="primary"
          >
            {credentials.handle}
          </Typography>
          <Typography variant="body2" color="textSecondary">
            {credentials.email}
          </Typography>
          <Typography variant="h6" color="textSecondary">
            Bio: {credentials.bio}
          </Typography>
          <Typography variant="h6" color="textSecondary">
            Website: {credentials.website}
          </Typography>{" "}
          <Typography variant="h6" color="textSecondary">
            Location: {credentials.location}
          </Typography>
          <div>
            <Button
              variant="contained"
              color="primary"
              onClick={this.handleOpen}
            >
              Edit User Data
            </Button>
            <Dialog
              open={this.state.open}
              onClose={this.handleClose}
              aria-labelledby="simple-modal-title"
              aria-describedby="simple-modal-description"
            >
              {body}
            </Dialog>
          </div>
        </CardContent>
      </Card>
    );
  }
}

export default withStyles(styles)(Profile);
